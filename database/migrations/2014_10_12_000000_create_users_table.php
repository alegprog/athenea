<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('firstname');
            $table->string('lastname');
            $table->string('identification');
            $table->string('email')->unique();
            $table->string('username')->unique();
            $table->string('local_phone')->nullable();
            $table->string('mobile_phone')->nullable();
            $table->string('address')->nullable();
            $table->string('avatar')->nullable();
            $table->enum('gender', ['male','female'])->default('male');
            $table->enum('type', ['individual', 'multiple', 'manager', 'master'])->default('individual');       
            $table->string('registration_token')->nullable();
            $table->string('password');
            $table->integer('registration_id')->nullable()->unsigned();
            $table->foreign('registration_id')->references('id')->on('users');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
