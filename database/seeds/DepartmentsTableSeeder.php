<?php

use Illuminate\Database\Seeder;
use App\Entities\Department;
use App\Entities\City;

class DepartmentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $departments=[
        	[
        	  'name'=>'BOGOTA D.C.',
        	  'cities'=>[
        	  	['name'=>'BOGOTA']
        	  ]
        	],
        	[
        		'name'=>'CUNDINAMARCA',
        		'cities'=>[
        	  		['name'=>'CHIA'],
        	  		['name'=>'FUNZA'],
        	  	]
        	],
        	[
        		'name'=>'META',
        		'cities'=>[
        	  		['name'=>'ACACIAS'],
        	  		['name'=>'FUNZA'],
        	  	]
        	],
        	[
        		'name'=>'VALLE',
        		'cities'=>[
        	  		['name'=>'CALI'],
        	  		['name'=>'TULUA'],
        	  	]
        	],
        ];

        foreach ($departments as $department) {
        	$depart = new Department();
        	$depart->name=$department['name'];
        	$depart->save();
        	foreach ($department['cities'] as $city) {
        		$cit = new City();
        		$cit->name=$city['name'];
        		$cit->department_id=$depart->id;
        		$cit->save();
        	}
        }
    }
}
