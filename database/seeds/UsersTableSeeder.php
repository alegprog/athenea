<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users=[

        	[ 
        		'identification'=>'789654321',
        		'firstname'=>'Ruth',
        		'lastname'=>'Contreras',
        		'email'=>'ruth@gmail.com',
        		'username'=>'rcontreras',
        		'type'=>'manager',
        		'gender'=>'female',
        	],
        	[
        		'identification'=>'987654321',
        		'firstname'=>'Alejandro',
        		'lastname'=>'Garcia',
        		'email'=>'alejandrogarcia15@gmail.com',
        		'username'=>'alejandro',
        		'type'=>'manager',
        		'gender'=>'male',
        	],
        	[	
        		'identification'=>'543216789',
        		'firstname'=>'Individual',
        		'lastname'=>'Sistema',
        		'email'=>'individual@gmail.com',
        		'username'=>'gindividual',
        		'type'=>'individual',
        		'gender'=>'male',
        	],
        	[	
        		'identification'=>'678954321',
        		'firstname'=>'Multiple',
        		'lastname'=>'Sistema',
        		'email'=>'multiple@gmail.com',
        		'username'=>'gmultiple',
        		'type'=>'multiple',
        		'gender'=>'male',
        	]

        ];

        foreach ($users as $user) {
        	$add= new User();
        	$add->firstname=$user['firstname'];
        	$add->lastname=$user['lastname'];
        	$add->username=$user['username'];
        	$add->identification=$user['identification'];
        	$add->email=$user['email'];
        	$add->password=bcrypt('123456');
        	$add->type=$user['type'];
        	$add->gender=$user['gender'];
        	$add->save();
        }
    }
}
