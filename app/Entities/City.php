<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    public function department(){
        return $this->belongsTo('App\Entities\Department');
    }
}
