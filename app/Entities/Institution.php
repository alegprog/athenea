<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Institution extends Model
{
    public function department(){
        return $this->belongsTo('App\Entities\Department');
    }

    public function city(){
        return $this->belongsTo('App\Entities\City');
    }

    public function manager(){
        return $this->belongsTo('App\User','manager_id','id');
    }
}
