<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    public function citys(){
        return $this->hasMany('App\Entities\City');
    }
}
