<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getTypeEsAttribute(){
      if($this->type=='manager'){
        return 'Gerente Khaval';
      }elseif($this->type=='multiple'){
        return 'Gerente Múltiple';
      }elseif($this->type=='individual'){
        return 'Gerente Individual';
      }
    }

    public function getViewAttribute(){
      if($this->type=='manager'){
        return '<span class="label label-success"><i class="fa fa-circle"></i> Vista Kaval</span>';
      }elseif($this->type=='multiple'){
        return '<span class="label label-warning"><i class="fa fa-circle"></i> Vista Múltiple</span>';
      }elseif($this->type=='individual'){
        return '<span class="label label-info"><i class="fa fa-circle"></i> Vista Individual</span>';
      }else{
        return '';
      }
    }

    public function institutions()
    {
          return $this->hasMany('App\Entities\Institution','manager_id');
    }
}
