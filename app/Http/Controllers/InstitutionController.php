<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Requests\Institution\StoreInstitutionRequest;
use App\Http\Requests\Institution\UpdateInstitutionRequest;
use App\Http\Requests\Institution\UpdateInstitutionIndividualRequest;
use App\User;
use App\Entities\Department;
use App\Entities\City;
use App\Entities\Institution;
use Yajra\Datatables\Datatables;
use Storage;
use Validator;

class InstitutionController extends Controller
{
    public function index(){
    	return view('dashboard.institution.index');
    }

    public function create(){
    	$departments= Department::get();
    	$citys=City::where('department_id',0)->get();
        $institutions=Institution::get();
        $manager_select=$institutions->pluck('manager_id');
        //dd($manager_select);
        if(auth()->user()->type=='manager'){
            $managers=User::select('id','avatar')->selectRaw("concat(identification,' - ',firstname,' ',lastname) as name")->whereIn('type',['individual','multiple'])->whereNotIn('id',$manager_select)->get();
        }else{
            $user_id=auth()->user()->id;
            $managers=User::select('id','avatar')
            ->selectRaw("concat(identification,' - ',firstname,' ',lastname) as name")
            ->whereIn('type',['individual'])
            ->where('registration_id',$user_id)
            ->whereNotIn('id',$manager_select)
            ->get();
        }
    	return view('dashboard.institution.create', compact('departments','citys','managers'));
    }

    public function store(StoreInstitutionRequest $request){
    	//dd($request,$request->all());

    	$department_id=$request->department_id;
        $city_id=$request->city_id;

        $query_city=City::where('id',$city_id)->where('department_id',$department_id)->first();

        if(!$query_city){
        	return redirect()->route('institution.edit',$id)->withInput();
        }

    	$institution = new Institution;
		$institution->name=$request->name; 
		$institution->address=$request->address;	 
		$institution->department_id=$request->department_id; 
		$institution->city_id=$request->city_id; 
        $institution->creator_id=auth()->user()->id;
        $institution->manager_id=$request->manager_id;
		 		
		if($institution->save()){
			if ($request->hasFile('logo')){
	         	$path = $request->file('logo')->store('institution'.'/'.$institution->id,'institution');
	         	$institution->logo=$path;
	         	$institution->save();
	        }
			return redirect()->route('institution.index')->with('success','Se ha agregado satisfactoriamente !');
		}


    }

    public function edit($id){

    	$institution=Institution::findOrFail($id);
    	$departments= Department::get();
    	$citys= City::where('department_id',$institution->department_id)->get();
        $institutions=Institution::get();
        $manager_select=$institutions->pluck('manager_id');
        $filtered = $manager_select->filter(function ($value, $key) use($institution) {
            return $value != $institution->manager_id;
        });

        $filtered->all();
        //$notIn=$manager_select->whereNotIn([$institution->manager_id])->toArray();
        //dd($filtered);
        if(auth()->user()->type=='manager'){
            $managers=User::select('id','avatar')->selectRaw("concat(identification,' - ',firstname,' ',lastname) as name")->whereIn('type',['individual','multiple'])->whereNotIn('id',$filtered)->get();
        }else{
            $user_id[]=auth()->user()->id;

            $user_id=auth()->user()->id;

            if($institution->manager_id==$user_id){
                $managers=User::select('id','avatar')
                ->selectRaw("concat(identification,' - ',firstname,' ',lastname) as name")
                ->whereIn('type',['individual'])
                ->where('registration_id',$user_id)
                ->whereNotIn('id',$filtered)            
                ->orWhere('id',$user_id)            
                ->get();
            }else{
                $managers=User::select('id','avatar')
                ->selectRaw("concat(identification,' - ',firstname,' ',lastname) as name")
                ->whereIn('type',['individual'])
                ->where('registration_id',$user_id)
                ->whereNotIn('id',$filtered)            
                ->get();  
            }
        }

    	//dd($institution);

    	return view('dashboard.institution.edit',compact('institution','departments','citys','managers'));

    }

    public function update(UpdateInstitutionRequest $request,$id){
    	//dd($request,$request->all());

    	$department_id=$request->department_id;
        $city_id=$request->city_id;

        $query_city=City::where('id',$city_id)->where('department_id',$department_id)->first();

        if(!$query_city){
        	return redirect()->route('institution.edit',$id)->withInput();
        }

    	$institution = Institution::findOrFail($id);
		$institution->name=$request->name; 
		$institution->address=$request->address;	 
		$institution->department_id=$request->department_id; 
		$institution->city_id=$request->city_id; 
        $institution->manager_id=$request->manager_id;	
		
		if($institution->save()){
			if ($request->hasFile('logo')){

				 $logo =$institution->logo;
		         if(Storage::disk('institution')->has($logo)){
		            Storage::disk('institution')->delete($logo);
		         }
	         	$path = $request->file('logo')->store('institution'.'/'.$institution->id,'institution');
	         	$institution->logo=$path;
	         	$institution->save();
	        }
			return redirect()->route('institution.index')->with('success','Se ha actualizado satisfactoriamente !');
		}


    }

    public function data(Datatables $datatables)
    {
     
        if(auth()->user()->type=='manager'){
            $institutions = Institution::with(['department' => function ($query) {          
            }])->with(['city' => function ($query) {          
            }])->with(['manager' => function ($query) { 
                //$query->selectRaw("concat(identification,' - ',firstname,' ',lastname) as name");     
            }])->get();
        }else{
            $user_id=auth()->user()->id;
            $institutions = Institution::with(['department' => function ($query) {          
            }])->with(['city' => function ($query) {          
            }])->with(['manager' => function ($query) { 
                //$query->selectRaw("concat(identification,' - ',firstname,' ',lastname) as name");     
            }])->where('creator_id',$user_id)->orWhere('manager_id',$user_id)->get();
        }     

	    return $datatables::of($institutions)->addColumn('select_action', function ($institution) {
	        return '<span class="pull-right"> <a href="'.route('institution.edit',$institution).'"><span style="font-size:18px;" class="fa fa-pencil text-info" title="editar"></span></a></span>';
	    })->addColumn('manager', function ($institution) {
            return $institution->manager->identification.'<br>'.$institution->manager->firstname.' '.$institution->manager->lastname;
        })->rawColumns(['select_action','manager'])->make(true);

    }

    public function editIndividual(){
        if(count(auth()->user()->institutions)>0){
            $institution = auth()->user()->institutions->first();
            $departments= Department::get();
            $citys= City::where('department_id',$institution->department_id)->get();
            return view('dashboard.institution.editIndividual',compact('institution','departments','citys'));
        }else{
            return redirect()->route('dashboard.index');
        }
    }

    public function updateIndividual(UpdateInstitutionIndividualRequest $request){
        //dd($request,$request->all());

        $department_id=$request->department_id;
        $city_id=$request->city_id;

        $query_city=City::where('id',$city_id)->where('department_id',$department_id)->first();

        if(!$query_city){
            return redirect()->route('institution.edit')->withInput();
        }

        $institution = Institution::findOrFail($request->id);
        $institution->name=$request->name; 
        $institution->address=$request->address;     
        $institution->department_id=$request->department_id; 
        $institution->city_id=$request->city_id;  
        
        if($institution->save()){
            if ($request->hasFile('logo')){

                 $logo =$institution->logo;
                 if(Storage::disk('institution')->has($logo)){
                    Storage::disk('institution')->delete($logo);
                 }
                $path = $request->file('logo')->store('institution'.'/'.$institution->id,'institution');
                $institution->logo=$path;
                $institution->save();
            }
            return redirect()->route('dashboard.index')->with('success','Se ha actualizado satisfactoriamente !');
        }


    }

    
}
