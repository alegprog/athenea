<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Entities\City;
use App\Entities\Department;
use App\Http\Requests\City\StoreCityRequest;
use App\Http\Requests\City\UpdateCityRequest;
use Yajra\Datatables\Datatables;

class CityController extends Controller
{
    public function index(){
    	return view('dashboard.city.index');
    }

    public function create(){
        $departments= Department::get();
    	return view('dashboard.city.create',compact('departments'));
    }

    public function store(StoreCityRequest $request){
    	//dd($request,$request->all());

    	$city = new City;
		$city->name=$request->name;
		$city->department_id=$request->department_id; 
		
		if($city->save()){			
			return redirect()->route('city.index')->with('success','Se ha agregado satisfactoriamente !');
		}

    }

    public function edit($id){

    	$city=City::findOrFail($id);
        $departments= Department::get();

    	return view('dashboard.city.edit',compact('city','departments'));

    }

    public function update(UpdateCityRequest $request,$id){
    	//dd($request,$request->all());

    	$city = City::findOrFail($id);
		$city->name=$request->name;
		$city->department_id=$request->department_id;
		
		if($city->save()){
			
			return redirect()->route('city.index')->with('success','Se ha actualizado satisfactoriamente !');
		}


    }

    public function data(Datatables $datatables)
    {
     
        $citys = City::with(['department' => function ($query) {          
        }])->get();      

	    return $datatables::of($citys)->addColumn('select_action', function ($city) {
	        return '<span class="pull-right"> <a href="'.route('city.edit',$city).'"><span style="font-size:18px;" class="fa fa-pencil text-info" title="editar"></span></a></span>';
	    })->rawColumns(['select_action'])->make(true);

    }

    public function list($id){

        $data = City::where('department_id', $id)
           ->select('id as value', 'name as text')
           ->get()
           ->toArray();

       //array_unshift($states,[]);

       return $data;
    }
}
