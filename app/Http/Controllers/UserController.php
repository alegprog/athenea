<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Http\Requests\User\StoreUserRequest;
use App\Http\Requests\User\UpdateUserRequest;
use Yajra\Datatables\Datatables;
use Storage;

class UserController extends Controller
{
    public function index(){
    	return view('dashboard.user.index');
    }

    public function create(){
    	$gender=['male'=>'Masculino','female'=>'Femenino'];
    	$type=['manager'=>'Gerente Khaval', 'multiple'=>'Gerente Múltiple', 'individual'=>'Gerente Individual'];
    	return view('dashboard.user.create',compact('gender','type'));
    }

    public function store(StoreUserRequest $request){
    	//dd($request,$request->all());

    	$user = new User;
		$user->firstname=$request->firstname; 
		$user->lastname=$request->lastname;	 
		$user->identification=$request->identification; 
		$user->email=$request->email;	 
		$user->username=$request->username;	 
		$user->local_phone=$request->local_phone;	 
		$user->mobile_phone=$request->mobile_phone;	 
		$user->address=$request->address; 	 
		$user->gender=$request->gender;
		if(auth()->user()->type=='manager'){
			$user->type=$request->type;	
		}else{
			$user->type='individual';
		}		 
		$user->password=bcrypt($request->password);
		$user->registration_id=auth()->user()->id;
		
		if($user->save()){
			if ($request->hasFile('avatar')){
	         	$path = $request->file('avatar')->store('user'.'/'.$user->id,'user');
	         	$user->avatar=$path;
	         	$user->save();
	        }
			return redirect()->route('user.index')->with('success','Se ha agregado satisfactoriamente !');
		}


    }

    public function edit($id){

    	$user=User::findOrFail($id);

    	$gender=['male'=>'Masculino','female'=>'Femenino'];
    	$type=['manager'=>'Gerente Khaval', 'multiple'=>'Gerente Múltiple', 'individual'=>'Gerente Individual'];

    	return view('dashboard.user.edit',compact('gender','type','user'));

    }

    public function update(UpdateUserRequest $request,$id){
    	//dd($request,$request->all());

    	$user = User::findOrFail($id);
		$user->firstname=$request->firstname; 
		$user->lastname=$request->lastname;	 
		$user->identification=$request->identification; 
		$user->email=$request->email;	 
		$user->username=$request->username;	 
		$user->local_phone=$request->local_phone;	 
		$user->mobile_phone=$request->mobile_phone;	 
		$user->address=$request->address; 	 
		$user->gender=$request->gender;
		if(auth()->user()->type=='manager'){
			$user->type=$request->type;	
		}else{
			$user->type='individual';
		} 
		if($request->password){
			$user->password=bcrypt($request->password);
		}		
		
		if($user->save()){
			if ($request->hasFile('avatar')){
				 $avatar =$user->avatar;
		         if(Storage::disk('user')->has($avatar)){
		            Storage::disk('user')->delete($avatar);
		         }
	         	$path = $request->file('avatar')->store('user'.'/'.$user->id,'user');
	         	$user->avatar=$path;
	         	$user->save();
	        }
			return redirect()->route('user.index')->with('success','Se ha actualizado satisfactoriamente !');
		}


    }

    public function data(Datatables $datatables)
    {

     	if(auth()->user()->type=='manager'){
        	$users = User::all();  
        }else{
        	$user_id=auth()->user()->id;
        	$users = User::where('registration_id',$user_id)->get(); 
        }    

	    return $datatables::of($users)->addColumn('select_action', function ($user) {
	        return '<span class="pull-right"> <a href="'.route('user.edit',$user).'"><span style="font-size:18px;" class="fa fa-pencil text-info" title="editar"></span></a></span>';
	    })->editColumn('type_es', function ($user) {
            if($user->type=='manager'){
                return '<span class="label label-success"><i class="fa fa-dot-circle-o"></i> '.$user->type_es.'</span>';
            }elseif($user->type=='multiple'){
                return '<span class="label label-warning"><i class="fa fa-dot-circle-o"></i> '.$user->type_es.'</span>';
            }elseif($user->type=='individual'){
                return '<span class="label label-info"><i class="fa fa-dot-circle-o"></i> '.$user->type_es.'</span>';
            }
        })->rawColumns(['select_action','type_es'])->make(true);

    }

}
