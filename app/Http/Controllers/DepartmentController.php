<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Entities\Department;
use App\Http\Requests\Department\StoreDepartmentRequest;
use App\Http\Requests\Department\UpdateDepartmentRequest;
use Yajra\Datatables\Datatables;
use Storage;

class DepartmentController extends Controller
{
    public function index(){
    	return view('dashboard.department.index');
    }

    public function create(){
    	return view('dashboard.department.create');
    }

    public function store(StoreDepartmentRequest $request){
    	//dd($request,$request->all());

    	$department = new Department;
		$department->name=$request->name; 
		
		if($department->save()){			
			return redirect()->route('department.index')->with('success','Se ha agregado satisfactoriamente !');
		}

    }

    public function edit($id){

    	$department=Department::findOrFail($id);

    	return view('dashboard.department.edit',compact('department'));

    }

    public function update(UpdateDepartmentRequest $request,$id){
    	//dd($request,$request->all());

    	$department = Department::findOrFail($id);
		$department->name=$request->name;
		
		if($department->save()){
			
			return redirect()->route('department.index')->with('success','Se ha actualizado satisfactoriamente !');
		}


    }

    public function data(Datatables $datatables)
    {
     
        $departments = Department::all();      

	    return $datatables::of($departments)->addColumn('select_action', function ($department) {
	        return '<span class="pull-right"> <a href="'.route('department.edit',$department).'"><span style="font-size:18px;" class="fa fa-pencil text-info" title="editar"></span></a></span>';
	    })->rawColumns(['select_action'])->make(true);

    }
}
