<?php

namespace App\Http\Requests\Institution;

use Illuminate\Foundation\Http\FormRequest;

class StoreInstitutionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>'required|min:4|unique:institutions,name',
            'address'=>'required|string|min:10|max:255',
            'department_id'=>'required|exists:departments,id',
            'city_id'=>'required|exists:cities,id',
            'manager_id'=>'required|exists:users,id',
            'logo'=>'nullable|image|mimes:jpeg,png,jpg|max:2048',
        ];
    }
}
