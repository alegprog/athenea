<?php

namespace App\Http\Requests\Institution;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateInstitutionIndividualRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        
        //dd($department_id,$city_id);
        return [
            'name'=>'required|min:4|unique:institutions,name,'.$this->get('id').',id',
            'address'=>'required|string|min:10|max:255',
            'department_id'=>'required|exists:departments,id',
            'city_id'=>'required|exists:cities,id',
            'logo'=>'nullable|image|mimes:jpeg,png,jpg|max:2048',
        ];
    }
}
