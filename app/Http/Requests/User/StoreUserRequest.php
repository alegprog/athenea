<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;

class StoreUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'firstname'=>'required|string',
            'lastname'=>'required|string',
            'identification'=>'required|digits_between:7,15|min:6|unique:users,identification',
            'username'=>'required|alpha_dash|between:4,16|unique:users,username',
            'email'=>'required|email|unique:users,email',
            'local_phone'=>'nullable|digits_between:7,12',
            'mobile_phone'=>'nullable|digits_between:7,12',
            'avatar'=>'nullable|image|mimes:jpeg,png,jpg|max:2048',
            'gender'=>'required|in:male,female',
            'type'=>'required|in:individual,multiple,manager',
            'password' => 'required|string|min:6|confirmed',
            'address'=>'nullable|string|min:10|max:255',
        ];
    }
}
