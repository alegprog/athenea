<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/


$this->get('login', 'Auth\LoginController@showLoginForm')->name('login');
$this->post('login', 'Auth\LoginController@login')->name('login.post');
$this->post('logout', 'Auth\LoginController@logout')->name('logout');
$this->get('getlogout', 'Auth\LoginController@getLogout')->name('getlogout');

Route::get('/', function () {
	return redirect()->route('login');
});

Route::middleware(['auth'])->prefix('dashboard')->group(function () {

Route::get('/', 'DashboardController@index')->name('dashboard.index');

Route::get('users', 'UserController@index')->name('user.index');
Route::get('user/create', 'UserController@create')->name('user.create');
Route::post('user/store', 'UserController@store')->name('user.store');
Route::get('user/data', 'UserController@data')->name('user.data');
Route::get('user/edit/{id}', 'UserController@edit')->name('user.edit');
Route::put('user/update/{id}', 'UserController@update')->name('user.update');

Route::get('institutions', 'InstitutionController@index')->name('institution.index');
Route::get('institution/create', 'InstitutionController@create')->name('institution.create');
Route::post('institution/store', 'InstitutionController@store')->name('institution.store');
Route::get('institution/data', 'InstitutionController@data')->name('institution.data');
Route::get('institution/edit/{id}', 'InstitutionController@edit')->name('institution.edit');
Route::put('institution/update/{id}', 'InstitutionController@update')->name('institution.update');

Route::get('institution/edit', 'InstitutionController@editIndividual')->name('institution.editIndividual');
Route::put('institution/update', 'InstitutionController@updateIndividual')->name('institution.updateIndividual');

Route::get('departments', 'DepartmentController@index')->name('department.index');
Route::get('department/create', 'DepartmentController@create')->name('department.create');
Route::post('department/store', 'DepartmentController@store')->name('department.store');
Route::get('department/data', 'DepartmentController@data')->name('department.data');
Route::get('department/edit/{id}', 'DepartmentController@edit')->name('department.edit');
Route::put('department/update/{id}', 'DepartmentController@update')->name('department.update');

Route::get('citys', 'CityController@index')->name('city.index');
Route::get('city/create', 'CityController@create')->name('city.create');
Route::post('city/store', 'CityController@store')->name('city.store');
Route::get('city/data', 'CityController@data')->name('city.data');
Route::get('city/edit/{id}', 'CityController@edit')->name('city.edit');
Route::put('city/update/{id}', 'CityController@update')->name('city.update');
Route::get('city/list/{id}', 'CityController@list')->name('city.list');

});
