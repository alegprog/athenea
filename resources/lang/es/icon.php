<?php
return [
  'add'=>'<i class="glyphicon glyphicon-plus color-add"></i>',
  'create'=>'<i class="glyphicon glyphicon-plus color-create"></i>',
  'new'=>'<i class="glyphicon glyphicon-plus-sign color-new"></i>',
  'store'=>'<i class="glyphicon glyphicon-plus-sign color-new"></i>',
  'list'=>'<i class="glyphicon glyphicon-th-list color-list"></i>',
  'edit'=>'<i class="glyphicon glyphicon-edit color-edit"></i>',
  'update'=>'<i class="glyphicon glyphicon-edit color-update"></i>',
  'save'=>'<i class="glyphicon glyphicon-pencil color-save"></i>',
  'delete'=>'<i class="glyphicon glyphicon-remove color-delete"></i>',
  'cancel'=>'<i class="glyphicon glyphicon-pencil color-cance"></i>',
  'dashboard'=>'<i class="glyphicon glyphicon-dashboard"></i>',
  'disable'=>'<i class="glyphicon glyphicon-ban-circle"></i>',
  'active'=>'<i class="glyphicon glyphicon-ok"></i>',
  'schedule'=>'<i class="glyphicon glyphicon-calendar"></i>',
];
