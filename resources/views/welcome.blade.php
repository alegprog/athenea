@extends('layouts.master')

@section('breadcrumb')
<!-- Content Header (Page header) -->
<section class="content-header">
    <!--section starts-->
    <h1>
        Fixed Header &amp; Menu
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="index.html">
                <i class="fa fa-fw ti-home"></i> Dashboard
            </a>
        </li>
        <li>
            <a href="layout_fixed.html#">Layouts</a>
        </li>
        <li class="active">
            <a href="layout_fixed.html">
                Fixed Header &amp; Menu
            </a>
        </li>
    </ol>
</section>
@endsection