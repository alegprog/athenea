<div class="col-sm-6">
    <div class="form-group {{ $errors->has('firstname') ? ' has-error' : '' }}">
        {!! Form::label('firstname', 'Nombre:', ['class' => 'col-sm-12 col-md-4 control-label']) !!}
        <div class="col-sm-12 col-md-8">
        {!! Form::text('firstname', null, ['class'=>'form-control', 'placeholder'=>'Nombre']) !!}
        @if ($errors->has('firstname'))
          <span class="help-block">
              <strong>{{ $errors->first('firstname') }}</strong>
          </span>
        @endif
        </div>
    </div>
</div>

<div class="col-sm-6">
    <div class="form-group {{ $errors->has('lastname') ? ' has-error' : '' }}">
        {!! Form::label('lastname', 'Apellido:', ['class' => 'col-sm-12 col-md-4 control-label']) !!}
        <div class="col-sm-12 col-md-8">
        {!! Form::text('lastname', null, ['class'=>'form-control', 'placeholder'=>'Apellido']) !!}
        @if ($errors->has('lastname'))
          <span class="help-block">
              <strong>{{ $errors->first('lastname') }}</strong>
          </span>
        @endif
        </div>
    </div>
</div>

<div class="clearfix hidden-xs"></div>

<div class="col-sm-6">
    <div class="form-group {{ $errors->has('identification') ? ' has-error' : '' }}">
        {!! Form::label('identification', 'Identificación:', ['class' => 'col-sm-12 col-md-4 control-label']) !!}
        <div class="col-sm-12 col-md-8">
        {!! Form::text('identification', null, ['class'=>'form-control', 'placeholder'=>'Documento de identidad']) !!}
        @if ($errors->has('identification'))
          <span class="help-block">
              <strong>{{ $errors->first('identification') }}</strong>
          </span>
        @endif
        </div>
    </div>
</div>

<div class="col-sm-6">
    <div class="form-group {{ $errors->has('username') ? ' has-error' : '' }}">
        {!! Form::label('username', 'Usuario:', ['class' => 'col-sm-12 col-md-4 control-label']) !!}
        <div class="col-sm-12 col-md-8">
        {!! Form::text('username', null, ['class'=>'form-control', 'placeholder'=>'Usuario']) !!}
        @if ($errors->has('username'))
          <span class="help-block">
              <strong>{{ $errors->first('username') }}</strong>
          </span>
        @endif
        </div>
    </div>
</div>

<div class="clearfix hidden-xs"></div>

<div class="col-sm-6">
    <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
        {!! Form::label('email', 'Correo:', ['class' => 'col-sm-12 col-md-4 control-label']) !!}
        <div class="col-sm-12 col-md-8">
        {!! Form::text('email', null, ['class'=>'form-control', 'placeholder'=>'Correo Electrónico']) !!}
        @if ($errors->has('email'))
          <span class="help-block">
              <strong>{{ $errors->first('email') }}</strong>
          </span>
        @endif
        </div>
    </div>
</div>

<div class="col-sm-6">
    <div class="form-group {{ $errors->has('gender') ? ' has-error' : '' }}">
        {!! Form::label('gender', 'Sexo:', ['class' => 'col-sm-12 col-md-4 control-label']) !!}
        <div class="col-sm-12 col-md-8">
        {!! Form::select('gender', $gender, null, ['class'=>'form-control','placeholder' => '--- Selecione ---']) !!}
        @if ($errors->has('gender'))
          <span class="help-block">
              <strong>{{ $errors->first('gender') }}</strong>
          </span>
        @endif
         </div>
    </div>
</div>

<div class="clearfix hidden-xs"></div>

<div class="col-sm-6">
    <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
        {!! Form::label('password', 'Contraseña:', ['class' => 'col-sm-12 col-md-4 control-label']) !!}
        <div class="col-sm-12 col-md-8">
        {!! Form::password('password', ['class'=>'form-control', 'placeholder'=>'Contraseña']) !!}
        @if ($errors->has('password'))
          <span class="help-block">
              <strong>{{ $errors->first('password') }}</strong>
          </span>
        @endif
        </div>
    </div>
</div>

<div class="col-sm-6">
    <div class="form-group {{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
        {!! Form::label('password_confirmation', 'Confirmar Contraseña:', ['class' => 'col-sm-12 col-md-4 control-label']) !!}
        <div class="col-sm-12 col-md-8">
        {!! Form::password('password_confirmation', ['class'=>'form-control', 'placeholder'=>'Confirmar Contraseña']) !!}
        @if ($errors->has('password_confirmation'))
          <span class="help-block">
              <strong>{{ $errors->first('password_confirmation') }}</strong>
          </span>
        @endif
        </div>
    </div>
</div>

<div class="clearfix hidden-xs"></div>

<div class="col-sm-6">
    <div class="form-group {{ $errors->has('local_phone') ? ' has-error' : '' }}">
        {!! Form::label('local_phone', 'Teléfono Local:', ['class' => 'col-sm-12 col-md-4 control-label']) !!}
        <div class="col-sm-12 col-md-8">
         {!! Form::text('local_phone', null, ['class'=>'form-control', 'placeholder'=>'Teléfono Local']) !!}
         @if ($errors->has('local_phone'))
          <span class="help-block">
              <strong>{{ $errors->first('local_phone') }}</strong>
          </span>
        @endif
         </div>
    </div>
</div>

<div class="col-sm-6">
    <div class="form-group {{ $errors->has('mobile_phone') ? ' has-error' : '' }}">
        {!! Form::label('mobile_phone', 'Teléfono Celular:', ['class' => 'col-sm-12 col-md-4 control-label']) !!}
        <div class="col-sm-12 col-md-8">
         {!! Form::text('mobile_phone', null, ['class'=>'form-control', 'placeholder'=>'Teléfono Celular']) !!}
         @if ($errors->has('mobile_phone'))
          <span class="help-block">
              <strong>{{ $errors->first('mobile_phone') }}</strong>
          </span>
        @endif
         </div>
    </div>
</div>  

<div class="clearfix hidden-xs"></div>        

<div class="col-sm-6">
    <div class="form-group {{ $errors->has('address') ? ' has-error' : '' }}">
        {!! Form::label('address', 'Dirección:', ['class' => 'col-sm-12 col-md-4 control-label']) !!}
        <div class="col-sm-12 col-md-8">
         {!! Form::textarea('address', null, ['class'=>'form-control', 'placeholder'=>'Dirección','rows'=>'2']) !!}
         @if ($errors->has('address'))
          <span class="help-block">
              <strong>{{ $errors->first('address') }}</strong>
          </span>
         @endif
         </div>
    </div>
</div>

 <div class="col-sm-6">
    <div class="form-group {{ $errors->has('avatar') ? ' has-error' : '' }}">
        {!! Form::label('avatar', 'Foto:', ['class' => 'col-sm-12 col-md-4 control-label']) !!}
        <div class="col-sm-12 col-md-8">
            <div class="fileinput fileinput-new" data-provides="fileinput">
                <div class="fileinput-new thumbnail"
                     style="width: 150px; height: 150px;">
                    @if(isset($user))
                        <img src="{{ $user->avatar ? asset('file/'.$user->avatar) : asset('assets/template/light/img/original.jpg') }}" alt="profile pic holder" style="width: 150px;">                	
                    @else
                    	<img src="{{asset('assets/template/light/img/original.jpg')}}" alt="profile pic holder">
                    @endif
                </div>
                <div class="fileinput-preview fileinput-exists thumbnail"
                     style="max-width: 150px; max-height: 150px;"></div>
                <div>
                        <span class="btn btn-default btn-file">
                        <span class="fileinput-new">Selecionar Imagen</span>
                        <span class="fileinput-exists">Cambiar</span>
                        <input id="pic" name="avatar" type="file"
                               class="form-control"/>
                        </span>
                    <a href="addnew_user.html#" class="btn btn-danger fileinput-exists"
                       data-dismiss="fileinput">Eliminar</a>
                </div>

                @if ($errors->has('avatar'))
                  <span class="help-block">
                      <strong>{{ $errors->first('avatar') }}</strong>
                  </span>
                 @endif

            </div>
        </div>
    </div>
</div>

{!! Form::hidden('type','individual') !!}

<div class="clearfix hidden-xs"></div>

