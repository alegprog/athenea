@extends('layouts.master')

@section('css')
    <link href="{{asset('assets/template/light/vendors/jasny-bootstrap/css/jasny-bootstrap.css')}}" type="text/css" rel="stylesheet">
@endsection

@section('js')
    <script src="{{asset('assets/template/light/vendors/jasny-bootstrap/js/jasny-bootstrap.js')}}" type="text/javascript"></script>
@endsection

@section('breadcrumb')
<!-- Content Header (Page header) -->
<section class="content-header">
    <!--section starts-->
    <h1>
        Editar Ciudad
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{route('dashboard.index')}}">
                <i class="fa fa-fw ti-home"></i> Dashboard
            </a>
        </li>
        <li>
            <a href="{{route('city.index')}}">Ciudades</a>
        </li>
        <li class="active">
            <a href="{{route('city.edit',$city)}}">
                Editar
            </a>
        </li>
    </ol>
</section>
@endsection

@section('content')
    <a class="btn btn-warning" href="{{route('city.index')}}">Listado</a>
    <div class="clearfix"></div>
    <div class="row" style="margin-top:15px;">
    <div class="col-lg-12">
        <div class="panel ">
            <div class="panel-heading">
                <h3 class="panel-title">
                    <i class="fa fa-fw fa-angle-double-right"></i> Datos de la Ciudad
                </h3>
                <span class="pull-right hidden-xs">
                {{--<i class="fa fa-fw ti-angle-up clickable"></i>
                <i class="fa fa-fw ti-close removepanel clickable"></i>--}}
            </span>
            </div>
            <div class="panel-body">
            <div>
                {!! Form::model($city,['route' => ['city.update', $city->id],'class'=>'form-horizontal']) !!}
                
                {{ method_field('PUT') }}
                <br>                          

                @include('dashboard.city.partials.form')
                
                <div class="col-sm-12 pull-right">                   
                    <div class="form-group form-actions pull-right" style="padding-right: 10px;">
                        <div class="">
                            <button type="submit" class="btn btn-success">Modificar</button>
                            {{--<button type="reset" class="btn btn-effect-ripple btn-default reset_btn1"> Limpiar </button>--}}
                            {!! Form::hidden('id',null) !!}
                        </div>
                    </div>
                </div>
                </div>
            	{!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection
