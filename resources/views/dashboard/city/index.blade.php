@extends('layouts.master')

@section('css')
    <link rel="stylesheet" type="text/css" href="{{asset('assets/template/light/vendors/datatables/css/dataTables.bootstrap.css')}}"/>
@endsection



@section('breadcrumb')
<!-- Content Header (Page header) -->
<section class="content-header">
    <!--section starts-->
    <h1>
        Listado de Ciudades
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{route('dashboard.index')}}">
                <i class="fa fa-fw ti-home"></i> Dashboard
            </a>
        </li>
        <li>
            <a href="{{route('department.index')}}">Ciudades</a>
        </li>
        <li class="active">
            <a href="{{route('department.index')}}">
                Listado
            </a>
        </li>
    </ol>
</section>
@endsection

@section('content')
    <a class="btn btn-primary" href="{{route('city.create')}}">Agregar</a>
    <div class="clearfix"></div>
    <div class="row" style="margin-top:15px;">
    <div class="col-lg-12">
        <div class="panel ">
            <div class="panel-heading">
                <h3 class="panel-title">
                    <i class="fa fa-fw fa-angle-double-right"></i> Ciudades
                </h3>
                <span class="pull-right hidden-xs">
                {{--<i class="fa fa-fw ti-angle-up clickable"></i>
                <i class="fa fa-fw ti-close removepanel clickable"></i>--}}
            </span>
            </div>
            <div class="panel-body">
                <table id="dataTable" class="table table-bordered table-striped" width="100%">
                    <thead>
                        <tr>
                            <th class="">Ciudad</th>
                            <th class="">Departamento</th>
                            <th class="" width="150">Acciones</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
    </div>
@endsection

@section('js')
    <script type="text/javascript" src="{{asset('assets/template/light/vendors/datatables/js/jquery.dataTables.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/template/light/vendors/datatables/js/dataTables.bootstrap.js')}}"></script>    

    <script type="text/javascript">
        var datatableProduct=$('#dataTable').DataTable({
           //"sDom": "<'row mb-1'<'col-sm-6'l><'col-sm-6'f>r>t<'row'<'col-sm-6'i><'col-sm-6 center'p>>",
           renderer: 'bootstrap',
            serverSide: true,
            processing: true,
            ajax: "{{route('city.data')}}",
            columns: [
                {data: 'name'},
                {data: 'department.name'},
                {data: 'select_action',orderable: false, searchable: false},
            ],
            //order: [[ 0 ]],
            lengthMenu:[10,20,50],
            pageLength:10,
            language: {
               processing:     "Procesando ...",
               search:         '<span class="glyphicon glyphicon-search"></span>',
               searchPlaceholder: "BUSCAR",
               lengthMenu:     "Mostrar _MENU_ Registros",
               info:           "Mostrando _START_ a _END_ de _TOTAL_ Registros",
               infoEmpty:      "Mostrando 0 a 0 de 0 Registros",
               infoFiltered:   "(filtrada de _MAX_ registros en total)",
               infoPostFix:    "",
               loadingRecords: "...",
               zeroRecords:    "No se encontraron registros coincidentes",
               emptyTable:     "No hay datos disponibles en la tabla",
               paginate: {
                   first:      "Primero",
                   previous:   "Anterior",
                   next:       "Siguiente",
                   last:       "Ultimo"
               },
               aria: {
                   sortAscending:  ": habilitado para ordenar la columna en orden ascendente",
                   sortDescending: ": habilitado para ordenar la columna en orden descendente"
               }
            }
        });
    </script>
    
@endsection