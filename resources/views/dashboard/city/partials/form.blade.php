<div class="clearfix hidden-xs"></div>

<div class="col-sm-6">
    <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
        {!! Form::label('name', 'Nombre:', ['class' => 'col-sm-12 col-md-4 control-label']) !!}
        <div class="col-sm-12 col-md-8">
         {!! Form::text('name', null, ['class'=>'form-control', 'placeholder'=>'Nombre de la ciudad']) !!}
         @if ($errors->has('name'))
          <span class="help-block">
              <strong>{{ $errors->first('name') }}</strong>
          </span>
        @endif
         </div>
    </div>
</div>  

<div class="col-sm-6">
    <div class="form-group  {{ $errors->has('department_id') ? ' has-error' : '' }}">
        {!! Form::label('department_id', 'Departamento:', ['class' => 'col-sm-12 col-md-4 control-label']) !!}
        <div class="col-sm-12 col-md-8">
        {!! Form::select('department_id', array_pluck($departments,'name','id'), null, ['class'=>'form-control','placeholder' => '--- Selecione ---']) !!}
        @if ($errors->has('department_id'))
          <span class="help-block">
              <strong>{{ $errors->first('department_id') }}</strong>
          </span>
        @endif
         </div>
    </div>
</div>

<div class="clearfix hidden-xs"></div>