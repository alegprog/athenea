<div class="col-sm-4 col-md-4">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">
                   <i class="fa fa-fw fa-angle-double-right"></i> Usuarios
                </h3>
            </div>
            <div class="panel-body">
                <div class="center-block"> 
		            <div class="btn-group">
		              <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
		                <i class="fa fa-fw fa-cog"></i> Acciones
		                <span class="caret"></span>
		                <span class="sr-only">Toggle Dropdown</span>
		              </button>
		              <ul class="dropdown-menu" role="menu">
		                <li><a href="{{route('user.create')}}"><i class="fa fa-fw fa-caret-right"></i> Agregar</a></li>
		                <li><a href="{{route('user.index')}}"><i class="fa fa-fw fa-caret-right"></i> Listado</a></li>
		              </ul>
		            </div> 
		        </div>
            </div>
        </div>
    </div>

    <div class="col-sm-4 col-md-4">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">
                   <i class="fa fa-fw fa-angle-double-right"></i> Instituciones
                </h3>
            </div>
            <div class="panel-body">
                <div class="center-block"> 
		            <div class="btn-group">
		              <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
		                <i class="fa fa-fw fa-cog"></i> Acciones
		                <span class="caret"></span>
		                <span class="sr-only">Toggle Dropdown</span>
		              </button>
		              <ul class="dropdown-menu" role="menu">
		                <li><a href="{{route('institution.create')}}"><i class="fa fa-fw fa-caret-right"></i> Agregar</a></li>
		                <li><a href="{{route('institution.index')}}"><i class="fa fa-fw fa-caret-right"></i> Listado</a></li>
		              </ul>
		            </div> 
		        </div>
            </div>
        </div>
    </div>
    