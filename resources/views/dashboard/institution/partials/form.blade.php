
<div class="clearfix hidden-xs"></div>

<div class="col-sm-6">

<div class="col-sm-12">
    <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
        {!! Form::label('name', 'Nombre:', ['class' => 'col-sm-12 col-md-4 control-label']) !!}
        <div class="col-sm-12 col-md-8">
         {!! Form::text('name', null, ['class'=>'form-control', 'placeholder'=>'Nombre del colegio']) !!}
         @if ($errors->has('name'))
          <span class="help-block">
              <strong>{{ $errors->first('name') }}</strong>
          </span>
        @endif
         </div>
    </div>
</div>  

<div class="clearfix hidden-xs"></div>

<div class="col-sm-12">
    <div class="form-group {{ $errors->has('address') ? ' has-error' : '' }}">
        {!! Form::label('address', 'Dirección:', ['class' => 'col-sm-12 col-md-4 control-label']) !!}
        <div class="col-sm-12 col-md-8">
         {!! Form::textarea('address', null, ['class'=>'form-control', 'placeholder'=>'Dirección','rows'=>'2']) !!}
         @if ($errors->has('address'))
          <span class="help-block">
              <strong>{{ $errors->first('address') }}</strong>
          </span>
         @endif
         </div>
    </div>
</div>

<div class="clearfix hidden-xs"></div>

<div class="col-sm-12">
    <div class="form-group  {{ $errors->has('department_id') ? ' has-error' : '' }}">
        {!! Form::label('department_id', 'Departamento:', ['class' => 'col-sm-12 col-md-4 control-label']) !!}
        <div class="col-xs-12 col-sm-12 col-md-8">
        {!! Form::select('department_id', array_pluck($departments,'name','id'), null, ['class'=>'form-control','placeholder' => '--- Selecione ---','id'=>'slt_department_id']) !!}
        
        @if ($errors->has('department_id'))
          <span class="help-block">
              <strong>{{ $errors->first('department_id') }}</strong>
          </span>
        @endif
         </div>
        {{--<div class="col-sm-1 col-xs-1 visible-xs visible-sm" >
          <button" class="btn btn-default" id="but" type="button" >B</button>
        </div>--}}
    </div>
</div>

<div class="col-sm-12">
    <div class="form-group  {{ $errors->has('city_id') ? ' has-error' : '' }}">
        {!! Form::label('city_id', 'Ciudad:', ['class' => 'col-sm-12 col-md-4 control-label']) !!}
        <div class="col-sm-12 col-md-8">
        {!! Form::select('city_id', old('department_id')? array_pluck($citys,'name','id') : array_pluck($citys,'name','id'), null, ['class'=>'form-control','placeholder' => '--- Selecione ---','id'=>'slt_city_id']) !!}
        @if ($errors->has('city_id'))
          <span class="help-block">
              <strong>{{ $errors->first('city_id') }}</strong>
          </span>
        @endif
         </div>
    </div>
</div>

<div class="clearfix hidden-xs"></div>

<div class="col-sm-12">
    <div class="form-group  {{ $errors->has('manager_id') ? ' has-error' : '' }}">
        {!! Form::label('manager_id', 'Gerente:', ['class' => 'col-sm-12 col-md-4 control-label']) !!}
        <div class="col-xs-12 col-sm-12 col-md-8">
        {!! Form::select('manager_id', array_pluck($managers,'name','id'), null, ['class'=>'form-control select2','placeholder' => '--- Selecione ---', 'id'=>'manager_id']) !!}
        
        @if ($errors->has('manager_id'))
          <span class="help-block">
              <strong>{{ $errors->first('manager_id') }}</strong>
          </span>
        @endif
         </div>
    </div>
</div>

<div class="clearfix hidden-xs"></div>

</div>

<div class="col-sm-6">

<div class="col-sm-12">
    <div class="form-group {{ $errors->has('logo') ? ' has-error' : '' }}">
        {!! Form::label('logo', 'Logo:', ['class' => 'col-sm-12 col-md-2 control-label']) !!}
        <div class="col-sm-12 col-md-8">
            <div class="fileinput fileinput-new" data-provides="fileinput">
                <div class="fileinput-new thumbnail"
                     style="width: 150px; height: auto;">
                    @if(isset($institution))
                        <img src="{{ $institution->logo ? asset('file/'.$institution->logo) : asset('assets/template/light/img/default-thumbnail.jpg') }}" alt="profile pic holder" style="width: 150px;">                	
                    @else
                    	<img src="{{asset('assets/template/light/img/default-thumbnail.jpg')}}" alt="profile pic holder">
                    @endif
                </div>
                <div class="fileinput-preview fileinput-exists thumbnail"
                     style="max-width: 150px; max-height: 150px;"></div>
                <div>
                        <span class="btn btn-default btn-file">
                        <span class="fileinput-new">Selecionar Logo</span>
                        <span class="fileinput-exists">Cambiar</span>
                        <input id="pic" name="logo" type="file"
                               class="form-control"/>
                        </span>
                    <a href="addnew_user.html#" class="btn btn-danger fileinput-exists"
                       data-dismiss="fileinput">Eliminar</a>
                </div>

                @if ($errors->has('logo'))
                  <span class="help-block">
                      <strong>{{ $errors->first('logo') }}</strong>
                  </span>
                 @endif

            </div>
        </div>
    </div>
</div>



{{--<div class="col-sm-6">
    <div class="form-group  {{ $errors->has('manager_id') ? ' has-error' : '' }}">
        {!! Form::label('manager_id', 'Gerente:', ['class' => 'col-sm-12 col-md-4 control-label']) !!}
        <div class="col-xs-12 col-sm-12 col-md-8">
        {!! Form::select('manager_id', array_pluck($managers,'name','id'), null, ['class'=>'form-control','placeholder' => '--- Selecione ---']) !!}
        
        @if ($errors->has('manager_id'))
          <span class="help-block">
              <strong>{{ $errors->first('manager_id') }}</strong>
          </span>
        @endif
         </div>
    </div>
</div>--}}

<div class="clearfix hidden-xs"></div>



{{--<div class="form-group{{ $errors->has('state_id') ? ' has-error' : '' }}">
                          {!! Form::label('state_id', trans('label.state'), ['class' => 'col-md-4 control-label']) !!}

                          <div class="col-md-6">
                            {!!Form::select('state_id', array_pluck($departments,'name','id'), null, ['placeholder' => '--- Select ---','class'=>'form-control','id'=>'slt-state'])!!}
                              @if ($errors->has('state_id'))
                                  <span class="help-block">
                                      <strong>{{ $errors->first('state_id') }}</strong>
                                  </span>
                              @endif
                          </div>

                      </div>

                      <div class="form-group{{ $errors->has('city_id') ? ' has-error' : '' }}">
                          {!! Form::label('city_id', trans('label.city'), ['class' => 'col-md-4 control-label']) !!}

                          <div class="col-md-6">
                            {!!Form::select('city_id', old('city_id') ? array_pluck($cities->citys,'name','id') : [], null, ['placeholder' => '--- Select ---','class'=>'form-control','id'=>'slt-city'])!!}
                              @if ($errors->has('city_id'))
                                  <span class="help-block">
                                      <strong>{{ $errors->first('city_id') }}</strong>
                                  </span>
                              @endif
                          </div>

                      </div>
<div class="clearfix hidden-xs"></div>--}}

</div>