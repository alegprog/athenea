@extends('layouts.master')

@section('css')
    <link href="{{asset('assets/template/light/vendors/jasny-bootstrap/css/jasny-bootstrap.css')}}" type="text/css" rel="stylesheet">
@endsection

@section('js')
    <script src="{{asset('assets/template/light/vendors/jasny-bootstrap/js/jasny-bootstrap.js')}}" type="text/javascript"></script>
@endsection

@section('breadcrumb')
<!-- Content Header (Page header) -->
<section class="content-header">
    <!--section starts-->
    <h1>
        Editar Institución
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{route('dashboard.index')}}">
                <i class="fa fa-fw ti-home"></i> Dashboard
            </a>
        </li>
        <li>
            <a href="{{route('institution.editIndividual')}}">Institución</a>
        </li>
        <li class="active">
            <a href="{{route('institution.editIndividual')}}">
                Editar
            </a>
        </li>
    </ol>
</section>
@endsection

@section('content')
    
    @php
    	if (!$errors->first('department_id')){
          if(old('department_id')){
           $citys = App\Entities\City::where('department_id', old('department_id'))
               ->select('id as id', 'name as name')
               ->get()
               ->toArray();
          }
        }

    @endphp

	<div class="clearfix"></div>
    <div class="row" style="margin-top:15px;">
    <div class="col-lg-12">
        <div class="panel ">
            <div class="panel-heading">
                <h3 class="panel-title">
                    <i class="fa fa-fw fa-angle-double-right"></i> Datos de la Institución
                </h3>
                <span class="pull-right hidden-xs">
                {{--<i class="fa fa-fw ti-angle-up clickable"></i>
                <i class="fa fa-fw ti-close removepanel clickable"></i>--}}
            </span>
            </div>
            <div class="panel-body">
            <div>
                
                {!! Form::model($institution,['route' => ['institution.updateIndividual'],'class'=>'form-horizontal','enctype'=>'multipart/form-data']) !!}

                {{ method_field('PUT') }}
                <br>                          

                @include('dashboard.institution.partials.formIndividual')
                
                <div class="col-sm-6 pull-right">                   
                    <div class="form-group form-actions pull-right" style="padding-right: 10px;">
                        <div class="">
                            <button type="submit" class="btn btn-success">Modificar</button>
                            {!! Form::hidden('id',null) !!} 
                        </div>
                    </div>
                </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
</div>
@endsection

@section('script')
    <script type="text/javascript">
        $(document).ready(function () {
            $.fn.populateSelect = function (values,slt,selected2=false) {
                 var options = '<option selected value>--- Selecione ---</option>';
                 $.each(values, function (key, row) {
                     options += '<option value="' + row.value + '">' + row.text + '</option>';
                 });
                 $(this).html(options);
                 if(selected2){
                    $('#select2-'+slt+'-container').html('--- Selecione ---');
                 }
           }

           $.fn.clearSelect = function (slt,selected2=false) {
             var options = '<option selected value>--- Selecione ---</option>';
             $('#'+slt).empty();
             $('#'+slt).html(options);
             if(selected2){
                $('#select2-'+slt+'-container').html('--- Selecione ---');
             }
           }

           $('#slt_department_id').change(function () {
             var slt_id = $(this).val();
             var url = '{{url("dashboard/city/list/")}}';
             if (slt_id== '') {
                 $('#slt_city_id').clearSelect('slt_city_id');
             } else {
                 $.getJSON(url+'/'+slt_id, null, function (values) {
                     $('#slt_city_id').populateSelect(values,'slt_city_id');
                 });
             }
           });

       });
    </script>
@endsection