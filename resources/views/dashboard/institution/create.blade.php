@extends('layouts.master')

@section('css')
    <link href="{{asset('assets/template/light/vendors/jasny-bootstrap/css/jasny-bootstrap.css')}}" type="text/css" rel="stylesheet">
    <link href="{{asset('assets/template/light/vendors/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('assets/template/light/vendors/select2/css/select2-bootstrap.css')}}" rel="stylesheet" type="text/css">
@endsection

@section('js')
    <script src="{{asset('assets/template/light/vendors/jasny-bootstrap/js/jasny-bootstrap.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/template/light/vendors/select2/js/select2.js')}}" type="text/javascript"></script>
@endsection

@section('breadcrumb')
<!-- Content Header (Page header) -->
<section class="content-header">
    <!--section starts-->
    <h1>
        Agregar Institución
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{route('dashboard.index')}}">
                <i class="fa fa-fw ti-home"></i> Dashboard
            </a>
        </li>
        <li>
            <a href="{{route('institution.index')}}">Instituciones</a>
        </li>
        <li class="active">
            <a href="{{route('institution.create')}}">
                Agregar
            </a>
        </li>
    </ol>
</section>
@endsection

@section('content')
    
    @php

        if (!$errors->first('department_id')){
          if(old('department_id')){
           $citys = App\Entities\City::where('department_id', old('department_id'))
               ->select('id as id', 'name as name')
               ->get()
               ->toArray();
          }
        }

    @endphp

	<a class="btn btn-warning" href="{{route('institution.index')}}">Listado</a>
    <div class="clearfix"></div>
    <div class="row" style="margin-top:15px;">
    <div class="col-lg-12">
        <div class="panel ">
            <div class="panel-heading">
                <h3 class="panel-title">
                    <i class="fa fa-fw fa-angle-double-right"></i> Datos de la Institución
                </h3>
                <span class="pull-right hidden-xs">
                {{--<i class="fa fa-fw ti-angle-up clickable"></i>
                <i class="fa fa-fw ti-close removepanel clickable"></i>--}}
            </span>
            </div>
            <div class="panel-body">
            <div>
                
                {!! Form::open(['route' => 'institution.store','class'=>'form-horizontal','enctype'=>'multipart/form-data']) !!}
                <br>                          

                @include('dashboard.institution.partials.form')
                
                <div class="col-sm-6"> 
                                      
                    <div class="form-group form-actions pull-right" style="padding-right: 10px;">
                        <div class="">
                            <button type="submit" class="btn btn-success">Guardar</button>
                            
                        </div>
                    </div>
                </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
</div>
@endsection

@section('script')
    <script type="text/javascript">
        var dataManagers = [
            @foreach($managers as $manager)
            { id: '{{$manager->id}}', text: '{{$manager->name}}', imagen: '{{$manager->avatar}}'},
            @endforeach
        ];

        $(document).ready(function () {
            $.fn.populateSelect = function (values,slt,selected2=false) {
                 var options = '<option selected value>--- Selecione ---</option>';
                 $.each(values, function (key, row) {
                     options += '<option value="' + row.value + '">' + row.text + '</option>';
                 });
                 $(this).html(options);
                 if(selected2){
                    $('#select2-'+slt+'-container').html('--- Selecione ---');
                 }
           }

           $.fn.clearSelect = function (slt,selected2=false) {
             var options = '<option selected value>--- Selecione ---</option>';
             $('#'+slt).empty();
             $('#'+slt).html(options);
             if(selected2){
                $('#select2-'+slt+'-container').html('--- Selecione ---');
             }
           }

       
            /*$(document).on('change','#slt_department_id',function(){ 
           //alert('hola');
           var slt_id = $(this).val();

             if (slt_id== '') {
                 $('#slt_city_id').clearSelect('slt_city_id');
             } else {
                 $.getJSON("/city/list/"+slt_id, null, function (values) {
                     $('#slt_city_id').populateSelect(values,'slt_city_id');
                 });
             }
        }); */

           

	   /*if ( /Android|webOS|iPhone|iPod|Blackberry|Windows Phone/i.test(navigator.userAgent)){
           alert('mobile');
                 
	   }else{
            $('#slt_department_id').change(function () {
             var slt_id = $(this).val();

             if (slt_id== '') {
                 $('#slt_city_id').clearSelect('slt_city_id');
             } else {
                 $.getJSON("/city/list/"+slt_id, null, function (values) {
                     $('#slt_city_id').populateSelect(values,'slt_city_id');
                 });
             }
           });
       }*/ 

            function formatState(manager) {
                if (!manager.id) {
                    return manager.text;
                }
                console.log(manager);
                /*var $manager = $(
                    '<span><img src="http://athenea.app/file/'+manager.imagen+'" class="img-flag"  width="20px" height="20px"></img> ' + manager.text + '</span>';
                );*/
                var $manager = $(
                '<img width="50" src="http://athenea.app/file/'+manager.imagen+'"></img>' +
                '<span class="flag-text"> '+ manager.text+"</span>"
               );
                return $manager;
            }

            {{--$("#manager_id").select2({
                templateResult: formatState,
                placeholder: "--- Selecione ---",                
                data: dataManagers,
                theme: "bootstrap"
            });--}}


            // Hacemos la lógica que cuando nuestro SELECT cambia de valor haga algo
        $("#slt-state").change(function(){

          $.ajaxSetup({
            headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
          });
            // Guardamos el select de cursos
            var city = $("#slt-city");

            // Guardamos el select de alumnos
            var state = $(this);
            console.log(state.val());
            var url = '{{url("/")}}';
            if($(this).val() != '')
            {
                $.ajax({                    
                    url:  url+'/city/list/'+state.val(),
                    type:  'GET',
                    dataType: 'json',
                    beforeSend: function ()
                    {
                        state.prop('disabled', true);
                        city.prop('disabled', true);
                    },
                    success:  function (r)
                    {
                        console.log(r);
                        state.prop('disabled', false);

                        // Limpiamos el select
                        city.find('option').remove();

                        $(r).each(function(i, v){ // indice, valor
                            city.append('<option value="' + v.id + '">' + v.text + '</option>');
                        })

                        city.prop('disabled', false);
                    },
                    error: function(r)
                    {
                        console.log(r);
                        alert('Ocurrio un error en el servidor ..');
                        state.prop('disabled', false);
                    }
                });
            }
            else
            {
                city.find('option').remove();
                city.prop('disabled', true);
            }
        })
       

       });

       function selectItemByValue(dropValg, value) {
          var denne = document.getElementById(dropValg);
          denne.value = value;
          denne.onchange();
        }

       window.onload = function () {
            var stateSel = document.getElementById("slt_department_id");
               stateSel.onchange = function(){ 
               var slt_id = $(this).val();
                var url = '{{url("dashboard/city/list/")}}';
                 if (slt_id== '') {
                     $('#slt_city_id').clearSelect('slt_city_id');
                 } else {
                     $.getJSON(url+'/'+slt_id, null, function (values) {
                         $('#slt_city_id').populateSelect(values,'slt_city_id');
                     });
                 }

                /*document.getElementById("but").onclick = function() {
                    selectItemByValue("slt_department_id", $("#slt_department_id").val());
                }*/
            }
        }


    </script>
@endsection
