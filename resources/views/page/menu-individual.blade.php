<li class="menu-dropdown">
    <a href="javascript:void(0);">
        <i class="menu-icon fa fa-fw fa-angle-double-right"></i>
        <span>
            Institución
        </span>
        <span class="fa arrow"></span>
    </a>
    <ul class="sub-menu">
        <li>
            <a href="{{route('institution.editIndividual')}}">
               <i class="fa fa-fw fa-caret-right"></i> Editar Institución
            </a>
        </li>                             
    </ul>
</li>